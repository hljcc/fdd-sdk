<?php

return [
	//通用接口调用地址，正式地址：https://textapi.fadada.com/api2
	'FddServer'            => 'https://test.api.fabigbig.com:8888/api',
	//法大大企业页面认证接口调用地址
	'FddServerSyncCompany' => 'https://partner-test.fadada.com',
	// 法大大存证服务地址
	'FDDWitnessServer'     => "https://czapi-test.fadada.com:7500/evidence-api",
	// AppId 接入方的ID
	'AppId'                => '',
	// AppSecret 接入方的密钥
	'AppSecret'            => '',
];
