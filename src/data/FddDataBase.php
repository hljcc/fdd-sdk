<?php
namespace fadadaApi\data;


use fadadaApi\FddException;

/**
 * 数据对象基础类，该类中定义数据类最基本的行为，包括：
 * 计算/设置/获取签名、输出xml格式的参数、从xml读取数据对象等
 *
 * @author widyhu
 */
class FddDataBase
{
    protected $values = [];

    /**
     * 输出xml字符
     * @throws FddException
     **/
    public function ToXml(): string
    {
        if (!is_array($this->values) || count($this->values) <= 0) {
            throw new FddException("数组数据异常！");
        }

        $xml = "<xml>";
        foreach ($this->values as $key => $val) {
            if (is_numeric($val)) {
                $xml .= "<" . $key . ">" . $val . "<" . $key . ">";
            } else {
                $xml .= "<" . $key . "><![CDATA[" . $val . "]]><" . $key . ">";
            }
        }
        $xml .= "</xml>";
        return $xml;
    }

	/**
	 * 将xml转为array
	 *
	 * @param string $xml
	 *
	 * @return array
	 * @throws \fadadaApi\FddException
	 */
    public function FromXml(string $xml): array
    {
        if (!$xml) {
            throw new FddException("xml数据异常！");
        }
        //将XML转为array
        //禁止引用外部xml实体
        libxml_disable_entity_loader(true);
        $this->values = json_decode(json_encode(simplexml_load_string($xml, "SimpleXMLElement", LIBXML_NOCDATA)), true);
        return $this->values;
    }

    /**
     * 格式化参数格式化成url参数
     */
    public function ToUrlParams(): string
    {
        $buff = "";
        foreach ($this->values as $k => $v) {
            if ($k != "sign" && $v != "" && !is_array($v)) {
                $buff .= $k . "=" . $v . "&";
            }
        }

	    return trim($buff, "&");
    }

    /**
     * 设置AppID
     * @param string $value
     **/
    public function SetApp_id($value)
    {
        $this->values["app_id"] = $value;
    }

    /**
     * 获取AppId
     * @return string
     **/
    public function GetApp_id(): string
    {
        return $this->values["app_id"];
    }

    /**
     * 判断AppId是否存在
     * @return true 或 false
     **/
    public function IsApp_idSet(): bool
    {
        return array_key_exists("organization", $this->values);
    }

    /**
     * 设置请求时间
     *
     * @param string $value
     **/
    public function SetTimestamp(string $value)
    {
        $this->values["timestamp"] = $value;
    }

    /**
     * 获取请求时间
     * @return string
     **/
    public function GetTimestamp(): string
    {
        return $this->values["timestamp"];
    }

    /**
     * 判断请求时间是否存在
     * @return true 或 false
     **/
    public function IsTimestampSet(): bool
    {
        return array_key_exists("timestamp", $this->values);
    }

    /**
     * 设置版本号
     *
     * @param string $value
     **/
    public function SetV(string $value)
    {
        $this->values["v"] = $value;
    }

    /**
     * 获取版本号
     * @return string
     **/
    public function GetV(): string
    {
        return $this->values["v"];
    }

    /**
     * 判断版本号是否存在
     * @return true 或 false
     **/
    public function IsVSet(): bool
    {
        return array_key_exists("v", $this->values);
    }

    /**
     * 设置消息摘要
     *
     * @param string $value
     **/
    public function SetMsg_digest(string $value)
    {
        $this->values["msg_digest"] = $value;
    }

    /**
     * 获取消息摘要
     * @return string
     **/
    public function GetMsg_digest(): string
    {
        return $this->values["msg_digest"];
    }

    /**
     * 判断消息摘要是否存在
     * @return true 或 false
     **/
    public function IsMsg_digestSet(): bool
    {
        return array_key_exists("msg_digest", $this->values);
    }

    /**
     * 获取设置的string
     */
    public function GetValues(): array
    {
        return $this->values;
    }
}

