<?php

namespace fadadaApi\data;

/**
 * 合规化方案 实名认证类
 * Class FddCertification
 */
class FddCertification extends FddDataBase
{
	/**
	 * 客户编号 注册账号时返回
	 *
	 * @param $value
	 */
	public function SetCustomerID($value)
	{
		$this->values['customer_id'] = $value;
	}

	/**
	 *  判断 客户编号 是否存在
	 *
	 * @return bool
	 */
	public function IsCustomerIDSet(): bool
	{
		return array_key_exists('customer_id', $this->values);
	}

	/**
	 * 实名认证套餐类型
	 *
	 * @param $value
	 */
	public function SetVerifiedWay($value)
	{
		$this->values['verified_way'] = $value;
	}

	/**
	 *  判断 实名认证套餐类型 是否存在
	 *
	 * @return bool
	 */
	public function IsVerifiedWaySet(): bool
	{
		return array_key_exists('verified_way', $this->values);
	}

	/**
	 * 管理员 实名认证套餐类型 0:三要素标准方案； 6补充三要素方案+人工审核
	 *
	 * @param $value
	 */
	public function SetMVerifieday($value)
	{
		$this->values['m_verified_way'] = $value;
	}

	/**
	 *  判断 管理员 实名认证套餐类型 是否存在
	 *
	 * @return bool
	 */
	public function IsMVerifiedaySet(): bool
	{
		return array_key_exists('m_verified_way', $this->values);
	}

	/**
	 * 是否允许用户页面修改 1允许，2不允许
	 *
	 * @param $value
	 */
	public function SetPageModify($value)
	{
		$this->values['page_modify'] = $value;
	}

	/**
	 *  判断 是否允许用户页面修改 是否存在
	 *
	 * @return bool
	 */
	public function IsPageModifySet(): bool
	{
		return array_key_exists('page_modify', $this->values);
	}

	/**
	 *  认证回调地址
	 *
	 * @param $value
	 */
	public function SetNotifyUrl($value)
	{
		$this->values['notify_url'] = $value;
	}

	/**
	 *  判断 认证回调地址 是否存在
	 *
	 * @return bool
	 */
	public function IsNotifyUrlSet(): bool
	{
		return array_key_exists('notify_url', $this->values);
	}

	/**
	 *  认证同步通知url
	 *
	 * @param $value
	 */
	public function SetReturnUrl($value)
	{
		$this->values['return_url'] = $value;
	}

	/**
	 *  判断 认证同步通知url 是否存在
	 *
	 * @return bool
	 */
	public function IsReturnUrlSet(): bool
	{
		return array_key_exists('return_url', $this->values);
	}

	/**
	 *  企业信息
	 *
	 * @param $value
	 */
	public function SetCompanyInfo($value)
	{
		$this->values['company_info'] = $value;
	}

	/**
	 *  判断 企业信息 是否存在
	 *
	 * @return bool
	 */
	public function IsCompanyInfo(): bool
	{
		return array_key_exists('company_info', $this->values);
	}

	/**
	 *  对公账号信息
	 *
	 * @param $value
	 */
	public function SetBankInfo($value)
	{
		$this->values['bank_info'] = $value;
	}

	/**
	 *  判断 对公账号信息 是否存在
	 *
	 * @return bool
	 */
	public function IsBankInfo(): bool
	{
		return array_key_exists('bank_info', $this->values);
	}

	/**
	 *  企业负责人身份: 1.法人，2 代理人
	 *
	 * @param $value
	 */
	public function SetCompanyPrincipalType($value)
	{
		$this->values['company_principal_type'] = $value;
	}

	/**
	 *  判断 企业负责人身份: 1.法人，2 代理人 是否存在
	 *
	 * @return bool
	 */
	public function IsCompanyPrincipalType(): bool
	{
		return array_key_exists('company_principal_type', $this->values);
	}

	/**
	 *  企业负责人身份: 1.法人，2 代理人
	 *
	 * @param $value
	 */
	public function SetLegalnfo($value)
	{
		$this->values['legal_info'] = $value;
	}

	/**
	 *  判断 企业负责人身份: 1.法人，2 代理人 是否存在
	 *
	 * @return bool
	 */
	public function IsLegalnfo(): bool
	{
		return array_key_exists('legal_info', $this->values);
	}

	/**
	 *  企业负责人身份: 1.法人，2 代理人
	 *
	 * @param $value
	 */
	public function SetAgentInfo($value)
	{
		$this->values['agent_info'] = $value;
	}

	/**
	 *  判断 企业负责人身份: 1.法人，2 代理人 是否存在
	 *
	 * @return bool
	 */
	public function IsAgentInfo(): bool
	{
		return array_key_exists('agent_info', $this->values);
	}

	/**
	 *  管理员 实名认证套餐类型
	 *
	 * @param $value
	 */
	public function SetMVerifiedWay($value)
	{
		$this->values['m_verified_way'] = $value;
	}

	/**
	 *  判断 实名认证套餐类型 是否存在
	 *
	 * @return bool
	 */
	public function IsMVerifiedWay(): bool
	{
		return array_key_exists('m_verified_way', $this->values);
	}


	/**
	 * 设置代理人姓名
	 *
	 * @param string $value
	 **/
	public function SetAgentName(string $value)
	{
		$this->values['agent_name'] = $value;
	}

	/**
	 * 获取代理人姓名
	 *
	 * @return string
	 **/
	public function GetAgentName():string
	{
		return $this->values['agent_name'];
	}

	/**
	 * 判断代理人姓名是否存在
	 *
	 * @return true 或 false
	 **/
	public function IsAgentName(): bool
	{
		return array_key_exists('agent_name', $this->values);
	}

	/**
	 * 设置代理人身份证号码
	 *
	 * @param string $value
	 **/
	public function SetAgentID(string $value)
	{
		$this->values['agent_id'] = $value;
	}

	/**
	 * 获取代理人身份证号码
	 *
	 * @return string
	 **/
	public function GetAgentID():string
	{
		return $this->values['agent_id'];
	}

	/**
	 * 判断代理人身份证号码是否存在
	 *
	 * @return true 或 false
	 **/
	public function IsAgentIDSet(): bool
	{
		return array_key_exists('agent_id', $this->values);
	}


	/**
	 * 设置代理人身份证号码
	 *
	 * @param string $value
	 **/
	public function SetAgentMobile(string $value)
	{
		$this->values['agent_mobile'] = $value;
	}

	/**
	 * 获取代理人身份证号码
	 *
	 * @return string
	 **/
	public function GetAgentMobile():string
	{
		return $this->values['agent_mobile'];
	}

	/**
	 * 判断代理人身份证号码是否存在
	 *
	 * @return true 或 false
	 **/
	public function IsAgentMobileSet(): bool
	{
		return array_key_exists('agent_mobile', $this->values);
	}


	/**
	 * 设置代理人身份证号码
	 *
	 * @param string $value
	 **/
	public function SetAgentIdFrontPath(string $value)
	{
		$this->values['agent_id_front_path'] = $value;
	}

	/**
	 * 获取代理人身份证号码
	 *
	 * @return string
	 **/
	public function GetAgentIdFrontPath():string
	{
		return $this->values['agent_id_front_path'];
	}

	/**
	 * 判断代理人身份证号码是否存在
	 *
	 * @return true 或 false
	 **/
	public function IsAgentIdFrontPath(): bool
	{
		return array_key_exists('agent_id_front_path', $this->values);
	}

	/**
	 * 设置代理人姓名
	 *
	 * @param string $value
	 **/
	public function SetLegal_name(string $value)
	{
		$this->values['legal_name'] = $value;
	}

	/**
	 * 获取法人姓名
	 *
	 * @return string
	 **/
	public function GetLegal_name():string
	{
		return $this->values['legal_name'];
	}

	/**
	 * 判断法人姓名是否存在
	 *
	 * @return true 或 false
	 **/
	public function IsLegal_nameSet(): bool
	{
		return array_key_exists('legal_name', $this->values);
	}

	/**
	 * 设置代理人姓名
	 *
	 * @param string $value
	 **/
	public function SetlegaldIFrontPath(string $value)
	{
		$this->values['legal_id_front_path'] = $value;
	}

	/**
	 * 获取法人姓名
	 *
	 * @return string
	 **/
	public function GetlegaldIFrontPath():string
	{
		return $this->values['legal_id_front_path'];
	}

	/**
	 * 判断法人姓名是否存在
	 *
	 * @return true 或 false
	 **/
	public function IslegaldIFrontPath(): bool
	{
		return array_key_exists('legal_id_front_path', $this->values);
	}

	/**
	 * 设置银行名称
	 *
	 * @param string $value
	 **/
	public function SetBankName(string $value)
	{
		$this->values['bank_name'] = $value;
	}

	/**
	 * 获取银行名称
	 *
	 * @return string
	 **/
	public function GetBankName():string
	{
		return $this->values['bank_name'];
	}

	/**
	 * 判断银行名称是否存在
	 *
	 * @return true 或 false
	 **/
	public function IsBankNameSet(): bool
	{
		return array_key_exists('bank_name', $this->values);
	}

	/**
	 * 设置银行账号
	 *
	 * @param string $value
	 **/
	public function SetBankId(string $value)
	{
		$this->values['bank_id'] = $value;
	}

	/**
	 * 获取银行账号
	 *
	 * @return string
	 **/
	public function GetBankId():string
	{
		return $this->values['bank_id'];
	}

	/**
	 * 判断银行账号是否存在
	 *
	 * @return true 或 false
	 **/
	public function IsBankIdSet(): bool
	{
		return array_key_exists('bank_id', $this->values);
	}

	/**
	 * 设置开户支行名称
	 *
	 * @param string $value
	 **/
	public function SetSubbranchName(string $value)
	{
		$this->values['subbranch_name'] = $value;
	}

	/**
	 * 获取开户支行名称
	 *
	 * @return string
	 **/
	public function GetSubbranchName():string
	{
		return $this->values['subbranch_name'];
	}

	/**
	 * 判断开户支行名称是否存在
	 *
	 * @return true 或 false
	 **/
	public function IsSubbranchNameSet(): bool
	{
		return array_key_exists('subbranch_name', $this->values);
	}

	/**
	 * 设置企业名称
	 *
	 * @param string $value
	 **/
	public function SetCompanyName(string $value)
	{
		$this->values['company_name'] = $value;
	}

	/**
	 * 获取企业名称
	 *
	 * @return string
	 **/
	public function GetCompanyName():string
	{
		return $this->values['company_name'];
	}

	/**
	 * 判断企业名称是否存在
	 *
	 * @return true 或 false
	 **/
	public function IsCompanyNameSet(): bool
	{
		return array_key_exists('company_name', $this->values);
	}

	/**
	 * 设置统一社会信用代码
	 *
	 * @param string $value
	 **/
	public function SetCreditNo(string $value)
	{
		$this->values['credit_no'] = $value;
	}

	/**
	 * 获取统一社会信用代码
	 *
	 * @return string
	 **/
	public function GetCreditNo():string
	{
		return $this->values['credit_no'];
	}

	/**
	 * 判断统一社会信用代码是否存在
	 *
	 * @return true 或 false
	 **/
	public function IsCreditNoSet(): bool
	{
		return array_key_exists('credit_no', $this->values);
	}

	/**
	 * 设置统一社会信用代码证件照路径
	 *
	 * @param string $value
	 **/
	public function SetCreditImagePath(string $value)
	{
		$this->values['credit_image_path'] = $value;
	}

	/**
	 * 获取统一社会信用代码证件照路径
	 *
	 * @return string
	 **/
	public function GetCreditImagePath():string
	{
		return $this->values['credit_image_path'];
	}

	/**
	 * 判断统一社会信用代码证件照路径是否存在
	 *
	 * @return true 或 false
	 **/
	public function IsCreditImagePathSet(): bool
	{
		return array_key_exists('credit_image_path', $this->values);
	}

	/**
	 * 设置法人姓名
	 *
	 * @param string $value
	 **/
	public function SetLegalName(string $value)
	{
		$this->values['legal_name'] = $value;
	}

	/**
	 * 获取法人姓名
	 *
	 * @return string
	 **/
	public function GetLegalName():string
	{
		return $this->values['legal_name'];
	}

	/**
	 * 判断法人姓名是否存在
	 *
	 * @return true 或 false
	 **/
	public function IsLegalNameSet(): bool
	{
		return array_key_exists('legal_name', $this->values);
	}

	/**
	 * 设置法人证件号（身份证）
	 *
	 * @param string $value
	 **/
	public function SetLegalId(string $value)
	{
		$this->values['legal_id'] = $value;
	}

	/**
	 * 获取法人证件号（身份证）
	 *
	 * @return string
	 **/
	public function GetLegalId():string
	{
		return $this->values['legal_id'];
	}

	/**
	 * 判断法人证件号（身份证）是否存在
	 *
	 * @return true 或 false
	 **/
	public function IsLegalIdSet(): bool
	{
		return array_key_exists('legal_id', $this->values);
	}

	/**
	 * 设置法人手机号（仅支持国内运营商）
	 *
	 * @param string $value
	 **/
	public function SetlegalMobile(string $value)
	{
		$this->values['legal_mobile'] = $value;
	}

	/**
	 * 获取法人手机号（仅支持国内运营商）
	 *
	 * @return string
	 **/
	public function GetlegalMobile():string
	{
		return $this->values['legal_mobile'];
	}

	/**
	 * 判断法人手机号（仅支持国内运营商）是否存在
	 *
	 * @return true 或 false
	 **/
	public function IslegalMobileSet(): bool
	{
		return array_key_exists('legal_mobile', $this->values);
	}

	/**
	 * 设置姓名
	 *
	 * @param string $value
	 **/
	public function SetCustomerName(string $value)
	{
		$this->values['customer_name'] = $value;
	}

	/**
	 * 获取姓名
	 *
	 * @return string
	 **/
	public function GetCustomerName(): string
	{
		return $this->values['customer_name'];
	}

	/**
	 * 判断姓名 是否存在
	 *
	 * @return true 或 false
	 **/
	public function IsCustomerName(): bool
	{
		return array_key_exists('customer_name', $this->values);
	}

	/**
	 * 设置证件类型 目前仅支持身份证-0
	 *
	 * @param string $value
	 **/
	public function SetCustomerIdentType(string $value)
	{
		$this->values['customer_ident_type'] = $value;
	}

	/**
	 * 获取证件类型
	 *
	 * @return string
	 **/
	public function GetCustomerIdentType(): string
	{
		return $this->values['customer_ident_type'];
	}

	/**
	 * 判断证件类型 是否存在
	 *
	 * @return true 或 false
	 **/
	public function IsCustomerIdentType(): bool
	{
		return array_key_exists('customer_ident_type', $this->values);
	}


	/**
	 * 设置证件类型 目前仅支持身份证-0
	 *
	 * @param string $value
	 **/
	public function SetCustomerIdentNo(string $value)
	{
		$this->values['customer_ident_no'] = $value;
	}

	/**
	 * 获取证件类型
	 *
	 * @return string
	 **/
	public function GetCustomerIdentNo() :string
	{
		return $this->values['customer_ident_no'];
	}

	/**
	 * 判断证件类型 是否存在
	 *
	 * @return true 或 false
	 **/
	public function IsCustomerIdentNo(): bool
	{
		return array_key_exists('customer_ident_no', $this->values);
	}

	/**
	 * 设置 手机号码
	 *
	 * @param string $value
	 **/
	public function SetMobile(string $value)
	{
		$this->values['mobile'] = $value;
	}

	/**
	 * 获取 手机号码
	 *
	 * @return string
	 */
	public function GetMobile(): string
	{
		return $this->values['mobile'];
	}

	/**
	 * 判断 手机号码 是否存在
	 *
	 * @return true 或 false
	 **/
	public function IsMobile(): bool
	{
		return array_key_exists('mobile', $this->values);
	}

	/**
	 * 设置 证件正面照下载地址
	 *
	 * @param string $value
	 **/
	public function SetIdentFrontPath(string $value)
	{
		$this->values['ident_front_path'] = $value;
	}

	/**
	 * 获取 证件正面照下载地址
	 *
	 * @return string
	 **/
	public function GetIdentFrontPath(): string
	{
		return $this->values['ident_front_path'];
	}

	/**
	 * 判断 证件正面照下载地址  是否存在
	 *
	 * @return true 或 false
	 **/
	public function IsIdentFrontPath(): bool
	{
		return array_key_exists('ident_front_path', $this->values);
	}


	/**
	 * 设置 实名认证序列号
	 *
	 * @param string $value
	 **/
	public function SetVerifiedVSerialNo(string $value)
	{
		$this->values['verified_serialno'] = $value;
	}

	/**
	 * 获取 实名认证序列号
	 *
	 * @return string
	 */
	public function GetVerifiedVSerialNo(): string
	{
		return $this->values['verified_serialno'];
	}

	/**
	 * 判断 实名认证序列号  是否存在
	 *
	 * @return true 或 false
	 **/
	public function IsVerifiedSerialNo(): bool
	{
		return array_key_exists('verified_serialno', $this->values);
	}

	/**
	 * 设置 uuid 查询认证结果时返回
	 *
	 * @param string $value
	 **/
	public function SetUUID(string $value)
	{
		$this->values['uuid'] = $value;
	}

	/**
	 * 获取 uuid 查询认证结果时返回
	 *
	 * @return string
	 **/
	public function GetUUID(): string
	{
		return $this->values['uuid'];
	}

	/**
	 * 判断uuid 查询认证结果时返回  是否存在
	 *
	 * @return true 或 false
	 **/
	public function IsUUID(): bool
	{
		return array_key_exists('uuid', $this->values);
	}

	public function SetResultType($value)
	{
		$this->values['result_type'] = $value;
	}

	public function IsResultTypeSet(): bool
	{
		return array_key_exists('result_type', $this->values);
	}

	public function SetCertFlag($value)
	{
		$this->values['cert_flag'] = $value;
	}

	public function IsCertFlagSet(): bool
	{
		return array_key_exists('cert_flag', $this->values);
	}

	public function SetOption($value)
	{
		$this->values['option'] = $value;
	}

	public function IsOptionSet(): bool
	{
		return array_key_exists('option', $this->values);
	}

	public function SetAuthorizationFile($value)
	{
		$this->values['authorization_file'] = $value;
	}

	public function IsAuthorizationFileSet(): bool
	{
		return array_key_exists('authorization_file', $this->values);
	}

	public function SetLang($value)
	{
		$this->values['lang'] = $value;
	}

	public function IsLangSet(): bool
	{
		return array_key_exists('lang', $this->values);
	}

	public function SetIdPhotoOptional($value)
	{
		$this->values['id_photo_optional'] = $value;
	}

	public function IsIdPhotoOptionalSet(): bool
	{
		return array_key_exists('id_photo_optional', $this->values);
	}

	public function SetOrganizationType($value)
	{
		$this->values['organization_type'] = $value;
	}

	public function IsOrganizationTypeSet(): bool
	{
		return array_key_exists('organization_type', $this->values);
	}

	public function SetEncryption($value)
	{
		$this->values['encryption'] = $value;
	}

	public function IsEncryptionSet(): bool
	{
		return array_key_exists('encryption', $this->values);
	}

	public function SetBankCardNo($value)
	{
		$this->values['bank_card_no'] = $value;
	}

	public function IsBankCardNoSet(): bool
	{
		return array_key_exists('bank_card_no', $this->values);
	}

	public function SetCertType($value)
	{
		$this->values['cert_type'] = $value;
	}

	public function IsCertTypeSet(): bool
	{
		return array_key_exists('cert_type', $this->values);
	}

	public function SetIsMiniProgram($value)
	{
		$this->values['is_mini_program'] = $value;
	}

	public function IsIsMiniProgramSet(): bool
	{
		return array_key_exists('is_mini_program', $this->values);
	}

	public function SetAreaCode($value)
	{
		$this->values['area_code'] = $value;
	}

	public function IsAreaCodeSet(): bool
	{
		return array_key_exists('area_code', $this->values);
	}

	/**
	 * 设置 指定管理员为"法人"身份下，允许的认证方式∶
	 * 1.法人身份认证;
	 * 2.对公打款认证;
	 * 3.纸质材料认证;
	 *
	 * @param $value
	 */
	public function SetLegal_allow_company_verify_way($value)
	{
		$this->values['legal_allow_company_verify_way'] = $value;
	}

	/**
	 * 设置 指定管理员为"代理人"身份下，允许的认证方式∶
	 * 1.法人授权认证;
	 * 2.对公打款认证;
	 * 3.纸质材料认证;
	 *
	 * @param $value
	 */
	public function SetAgent_allow_company_verify_way($value)
	{
		$this->values['agent_allow_company_verify_way'] = $value;
	}

	/**
	 * 设置 代理人证件正面照
	 *
	 * @param $value
	 */
	public function SetAgent_id_front_img($value)
	{
		$this->values['agent_id_front_img'] = $value;
	}

	/**
	 * 设置 法人证件正面照
	 *
	 * @param $value
	 */
	public function SetLegal_id_front_img($value)
	{
		$this->values['legal_id_front_img'] = $value;
	}

	/**
	 * 设置 银行所在省份
	 *
	 * @param $value
	 */
	public function SetBank_province_name($value)
	{
		$this->values['bank_province_name'] = $value;
	}

	/**
	 * 设置 银行所在市
	 *
	 * @param $value
	 */
	public function SetBank_city_name($value)
	{
		$this->values['bank_city_name'] = $value;
	}

	/**
	 * 设置 法人授权手机号
	 *
	 * @param $value
	 */
	public function SetLegal_authorized_mobile($value)
	{
		$this->values['legal_authorized_mobile'] = $value;
	}

	/**
	 * 设置 银行卡号
	 *
	 * @param $value
	 */
	public function SetBank_card_no($value)
	{
		$this->values['bank_card_no'] = $value;
	}

	/**
	 * 获取 银行卡号
	 *
	 * @return string
	 */
	public function getBank_card_no():string
	{
		return $this->values['bank_card_no'];
	}

	/**
	 * 设置 证件正面照图片文件
	 * cert_type=0:身份证正面
	 * cert_type=1:护照带人像图片
	 * cert_type=B:港澳居民来往内地通行证照带人像图片
	 * cert_type=C:台湾居民来往大陆通行证照带人像图片
	 *
	 * @param $value
	 */
	public function SetIdent_front_img($value)
	{
		$this->values['ident_front_img'] = $value;
	}

	/**
	 * 设置 证件反面照图片文件
	 * cert_type=0:身份证反面
	 * cert_type=1:护照封图片
	 * cert_type=B:港澳居民来往内地通行证照封图图片
	 * cert_type=C:台湾居民来往大陆通行证照封图图片
	 *
	 * @param $value
	 */
	public function SetIdent_back_img($value)
	{
		$this->values['ident_back_img'] = $value;
	}

	/**
	 * 设置 海外用户是否支持银行卡认证：0-否，1-是，
	 * 当接口中该参数传入有值时，以接口传入的配置为准，否则则取运营后台配置；
	 *
	 * @param $value
	 */
	public function SetIs_allow_overseas_bank_card_auth($value)
	{
		$this->values['is_allow_overseas_bank_card_auth'] = $value;
	}

	/**
	 * 设置 0：图片（默认图片）
	 * 1：pdf (仅支持企业申请表模板)
	 *
	 * @param string $value
	 **/
	public function SetDoc_type(string $value)
	{
		$this->values['doc_type'] = $value;
	}

	/**
	 * 判断 0：图片（默认图片）
	 * 1：pdf (仅支持企业申请表模板) 是否存在
	 *
	 * @return bool
	 */
	public function IsDoc_typeSet(): bool
	{
		return array_key_exists('doc_type', $this->values);
	}
}