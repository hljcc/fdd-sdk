<?php

namespace fadadaApi\data;



/**
 * 模板填充的动态表格参数实体
 */
class FddTemplateDynamicTable {

    /**
     * 动态表格插入方式:0：新建页面添加table（默认） 1：在某个关键字后添加table
     */
    protected int $insertWay =0;

    /**
     * 关键字方式插入动态表格:1. 当insertWay为1时，必填
     *                          2. 要求该关键字后（当前页）必须不包含内容，否则会被覆盖
     *                          3. 若关键字为多个 ，则取第一个关键字，在此关键字后插入table
     */
    protected $keyword;

    /**
     * 表格需要插入的页数:1. 当insertWay为0时，必填
     *                       2. 表示从第几页开始插入表格，如要从末尾插入table,则pageBegin为pdf总页数加1
     *                       3. 多个表格指定相同pageBegin，则多个表格按顺序插入，一个表格新起一页
     *                       4. pageBegin为-1时，则从pdf末尾插入table
     */
    protected int $pageBegin;

    /**
     * table是否有边框:true：有（默认）   false：无边框
     */
    protected bool $borderFlag =true;

    /**
     * 正文行高（表头不受影响）:单位：pt，即point，等于1/72英寸
     */
    protected float $cellHeight;

    /**
     * Table中每个单元的水平对齐方式:0：居左；1：居中；2：居右  默认为0
     */
    protected int $cellHorizontalAlignment =0;

    /**
     * Table中每个单元的垂直对齐方式: (4：居上；5：居中；6：居下)   默认为4
     */
    protected int $cellVerticalAlignment =4;

    /**
     * 表头上方的一级标题
     */
    protected string $theFirstHeader;

    /**
     * 表头信息: 类型是 Array[String]
     */
    protected array $headers;

    /**
     * 表头对齐方式:(0居左；1居中；2居右)   默认0
     */
    protected int $headersAlignment =0;

    /**
     * 正文: 类型是 Array[Array[String]]。(外层表示行，内层表示列)
     */
    protected array $datas;

    /**
     * 各列宽度比例: 类型是 Array[Integer]。 默认值：各列1:1
     */
    protected array $colWidthPercent=[];

    /**
     * table的水平对齐方式: (0居左，1居中，2居右) 默认1
     */
    protected int $tableHorizontalAlignment =1;

    /**
     * table宽度的百分比: (0<tableWidthPercentage<=100) 默认为100.0
     */
    protected float $tableWidthPercentage =100.0;

    /**
     * 设置table居左居中居右后的水平偏移量: (向右偏移值为正数，向左偏移值为负数)默认为0.0，单位px(像素)
     */
    protected float $tableHorizontalOffset =0.0;


    function SetInsertWay($value){
        $this->insertWay = $value;
    }

    function GetInsertWay(): int
    {
        return $this->insertWay;
    }

    function SetKeyword($value){
        $this->keyword = $value;
    }

    function GetKeyword(){
        return $this -> keyword;
    }

    function SetPageBegin($value){
        $this->pageBegin = $value;
    }

    function GetPageBegin(): int
    {
        return $this -> pageBegin;
    }

    function SetBorderFlag($value){
        $this-> borderFlag = $value;
    }

    function GetBorderFlag(): bool
    {
        return $this -> borderFlag;
    }

    function SetCellHeight($value){
        $this-> cellHeight = $value;
    }

    function GetCellHeight(): float
    {
        return $this -> cellHeight;
    }

    function SetCellHorizontalAlignment($value){
        $this-> cellHorizontalAlignment = $value;
    }

    function GetCellHorizontalAlignment(): int
    {
        return $this -> cellHorizontalAlignment;
    }

    function SetCellVerticalAlignment($value){
        $this-> cellVerticalAlignment = $value;
    }

    function GetCellVerticalAlignment(): int
    {
        return $this -> cellVerticalAlignment;
    }

    function SetTheFirstHeader($value){
        $this-> theFirstHeader = $value;
    }

    function GetTheFirstHeader(): string
    {
        return $this -> theFirstHeader;
    }

    function SetHeaders($value){
        $this-> headers = $value;
    }

    function GetHeaders(): array
    {
        return $this -> headers;
    }

    function SetHeadersAlignment($value){
        $this-> headersAlignment = $value;
    }

    function GetHeadersAlignment(): int
    {
        return $this -> headersAlignment;
    }

    function SetDatas($value){
        $this-> datas = $value;
    }

    function GetDatas(): array
    {
        return $this -> datas;
    }

    function SetColWidthPercent($value){
        $this-> colWidthPercent = $value;
    }

    function GetColWidthPercent(): array
    {
        return $this -> colWidthPercent;
    }

    function SetTableHorizontalAlignment($value){
        $this-> tableHorizontalAlignment = $value;
    }

    function GetTableHorizontalAlignment(): int
    {
        return $this -> tableHorizontalAlignment;
    }

    function SetTableWidthPercentage($value){
        $this-> tableWidthPercentage = $value;
    }

    function GetTableWidthPercentage(): float
    {
        return $this -> tableWidthPercentage;
    }

    function SetTableHorizontalOffset($value){
        $this-> tableHorizontalOffset = $value;
    }

    function GetTableHorizontalOffset(): float
    {
        return $this -> tableHorizontalOffset;
    }
}
