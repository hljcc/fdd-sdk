<?php
namespace fadadaApi\data;

/**
 * 合同管理类
 * Class FddContractManageMent
 */
class FddContractManageMent extends FddDataBase
{
    /**
     * 设置 合同编号
     *
     * @param string $value
     **/
    public function SetContract_id(string $value)
    {
        $this->values['contract_id'] = $value;
    }

	/**
	 * 获取 签署时所传合同编号
	 *
	 * @return string
	 */
    public function GetContract_id(): string
    {
        return $this->values['contract_id'];
    }

    /**
     * 判断 签署时所传合同编号 是否存在
     * @return true 或 false
     **/
    public function IsContract_idSet(): bool
    {
        return array_key_exists('contract_id', $this->values);
    }

    /**
     * 设置 合同编号（多）
     *
     * @param string $value
     **/
    public function SetContract_ids(string $value)
    {
        $this->values['contract_ids'] = $value;
    }

    /**
     * 判断 签署时所传合同编号 是否存在
     * @return true 或 false
     **/
    public function IsContract_idsSet(): bool
    {
        return array_key_exists('contract_ids', $this->values);
    }

    /**
     * 设置用户ID
     *
     * @param string $value
     **/
    public function SetCustomer_id(string $value)
    {
        $this->values['customer_id'] = $value;
    }

    /**
     * 设置 有效期
     *
     * @param string $value
     **/
    public function SetValidity(string $value)
    {
        $this->values['validity'] = $value;
    }

	/**
	 * 判断 有效期 是否存在
	 *
	 * @return bool
	 */
    public function IsValiditySet(): bool
    {
        return array_key_exists('validity', $this->values);
    }

    /**
     * 设置 有效次数
     *
     * @param string $value
     **/
    public function SetQuantity(string $value)
    {
        $this->values['quantity'] = $value;
    }

    /**
     * 判断 有效次数 是否存在
     * @return true 或 false
     **/
    public function IsQuantitySet(): bool
    {
        return array_key_exists('quantity', $this->values);
    }
}