# 法大大PHPSDK扩展包

#### 介绍
法大大PHPSDK扩展包

#### 软件架构
* 采用原版法大大PHPSDK包进行拆解，原版地址：https://static.fadada.com/openapi/OpenApi_v2实名认证方案-PHP.zip
* 扩展包适用于单用户和多用户


#### 安装教程

```shell
composer require hljcc/fdd-sdk
```

#### 使用说明

1.  若是单用户使用，直接修改 `config/fadada.php` 文件中的 `AppId` 和 `AppSecret`
2.  若是多用户，在实例化时，传入 `appId` 和 `appSecret` 即可

#### 官方API接口手册

1.  https://open.fadada.com/index.html#/portal/documentCenter/
